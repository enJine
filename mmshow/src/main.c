/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */
#include <ctype.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <SDL/SDL.h>

#include "libvideo/video.h"


#define XTILES  14
#define YTILES  8

static unsigned char map[32*64];
static unsigned char tiles[6*16*4];


static const unsigned char ttypes[16] = {
  0x33,  // 0x00
  0x01,  // 0x01
  0x11,  // 0x02
  0x12,  // 0x03
  0x30,  // 0x04
  0x23,  // 0x05
  0x02,  // 0x06
  0x03,  // 0x07
  0x32,  // 0x08
  0x22,  // 0x09
  0x00,  // 0x0A
  0x21,  // 0x0B
  0x10,  // 0x0C
  0x13,  // 0x0D
  0x31,  // 0x0E
  0x20,  // 0x0F
};


static void putTile (int trow, int tnum, int px, int py) {
  int x, y, d, a;
  //
  a = trow*(6*16)+tnum*2;
  for (y = 0; y < 16; ++y) {
    for (x = 0; x < 2; ++x) {
      unsigned char b = tnum<3 ? tiles[a+x] : 0;
      for (d = 7; d >= 0; --d, b >>= 1) putPixel(px+x*8+d, py+y, b&0x01?7:0);
    }
    a += 6;
  }
}


static void put2XTiles (unsigned char mapb, int px, int py) {
  int trow = (mapb>>4)&0x03;
  int tnum = ttypes[mapb&0x0f];
  putTile(trow, (tnum>>4)&0x03, px, py);
  putTile(trow, tnum&0x03, px+16, py);
}


static void drawMap (int mx, int my) {
  int dx, dy;
  char buf[16];
  //
  for (dy = 0; dy < YTILES; ++dy) {
    int ox = mx;
    for (dx = 0; dx < XTILES/2; ++dx) {
      put2XTiles(map[my*32+mx], dx*32, dy*16);
      sprintf(buf, "%02X", map[my*32+mx]);
      drawOutlineText(dx*32+2, dy*16+2, buf, 15, 0);
      mx = (mx+1)%32;
    }
    mx = ox; my = (my+1)%64;
  }
}


int main (int argc, char *argv[]) {
  static int mapX = 0;
  static int mapY = 0;
  //
  static SDL_Event event;
  static FILE *fl;
  //static char fname[128];
  //static char buf[1024];
  //int ctrlDown, ta;

  if ((fl = fopen("../gfx/tiles/blk_ded8_blobs.bin", "rb")) == NULL) { fprintf(stderr, "tiles?\n"); return 1; }
  fread(tiles, sizeof(tiles), 1, fl);
  fclose(fl);

  if ((fl = fopen("../maps/map00.bin", "rb")) == NULL) { fprintf(stderr, "map?\n"); return 1; }
  fread(map, sizeof(map), 1, fl);
  fclose(fl);

  videoInit();
  if (setVMode(2, 0)) { videoDeinit(); return 1; }
  setCaption("enJine level renderer");
  SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

/*
  drawOutlineText(10, 10, "test!", 15, 3);
  drawEllipse(8, 8, 8+6*5+4, 8+8+4, 10);
  drawCircle(45, 70, 42, 5);
  drawIconById(ICON_ID_PENTA, 80, 90);
  drawFilledEllipse(8+40, 8+40, 8+6*5+4+40, 8+8+4+40, 11);
  drawFilledCircle(120, 150, 42, 5);
  drawLine(50, 50, 100, 60, 7);
  putPixel(49, 49, 3);
  putPixel(101, 61, 3);

  drawCursor(8, 8, 0);
  drawCursor(28, 28, 1);
*/

repaint:
  drawBar(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
  drawMap(mapX, mapY);
  repaintScreen();

  for (;;) {
    while (SDL_WaitEvent(&event)) {
      switch (event.type) {
        case SDL_QUIT: goto quit;
        case SDL_VIDEOEXPOSE: repaintScreen(); break;
        case SDL_KEYDOWN:
          //ctrlDown = event.key.keysym.mod&KMOD_CTRL;
          switch (event.key.keysym.sym) {
            case SDLK_ESCAPE: case SDLK_q: goto quit;
            case SDLK_LEFT: if (mapX > 0) --mapX; goto repaint;
            case SDLK_RIGHT: if (mapX < 31) ++mapX; goto repaint;
            case SDLK_UP: if (mapY > 0) --mapY; goto repaint;
            case SDLK_DOWN: if (mapY < 63) ++mapY; goto repaint;
            default: ;
          }
          break;
        default: ;
      }
    }
  }

quit:
  videoDeinit();
  return 0;
}
